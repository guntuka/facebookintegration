//
//  ViewController.h
//  facebookDemo
//
//  Created by Ramoji Krian on 05/06/2560 BE.
//  Copyright © 2560 BE Ramoji Krian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *nameTF;
@property (strong, nonatomic) IBOutlet UITextField *emailTF;
@property (strong, nonatomic) IBOutlet UITextField *idNoTF;
- (IBAction)submitBtn:(id)sender;

@property (strong, nonatomic) IBOutlet FBSDKLoginButton *fbloginBtn;


@end


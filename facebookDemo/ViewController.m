//
//  ViewController.m
//  facebookDemo
//
//  Created by Ramoji Krian on 05/06/2560 BE.
//  Copyright © 2560 BE Ramoji Krian. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ViewController ()<FBSDKLoginButtonDelegate>

@end

@implementation ViewController
@synthesize nameTF,emailTF,idNoTF,fbloginBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    fbloginBtn = [[FBSDKLoginButton alloc] init];
    fbloginBtn.hidden=YES;
    fbloginBtn.delegate=self;
    fbloginBtn.readPermissions = @[@"public_profile",@"email",@"user_friends"];
    
    NSLog(@"facebook integration saved in bitbucket");
    
    
    
    
    
    
    

}
-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error
{
    if ([FBSDKAccessToken currentAccessToken]) {
        
  NSLog(@"facebook integration saved in bitbucket");
    }
}
-(BOOL)loginButtonWillLogin:(FBSDKLoginButton *)loginButton
{
    [self toggleHiddenState:YES];
    return YES;
}
-(void)toggleHiddenState:(BOOL)shouldHide{
    
}


- (IBAction)submitBtn:(id)sender {
    
    [fbloginBtn sendActionsForControlEvents: UIControlEventTouchUpInside];
    
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id,name,email" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
     
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             NSLog(@"fetched user:%@  and Email : %@", result,result[@"email"]);
         }
         
         
         nameTF.text=[result objectForKey:@"name"];
         emailTF.text=[result objectForKey:@"email"];
         idNoTF.text=[result objectForKey:@"id"];
         
     }];
    

     }
-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    NSLog(@"logout button is pressed here ******* ");
    
}

@end
